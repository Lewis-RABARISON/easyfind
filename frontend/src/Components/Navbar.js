import React, { Fragment, useContext } from 'react'
import logo from './logo.svg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { NavLink } from 'react-router-dom'
import SecurityApi from '../Services/UserApi'
import AuthenticationContext from '../Contexts/AuthenticationContext'

function Navbar({ history}) {

  const {isAuthenticated, setIsAuthenticated} = useContext(AuthenticationContext);

  function handleLogout() {
    SecurityApi.logout()
    setIsAuthenticated(false);
    history.push("/connexion");
  }

  function handleSaleProduct() {
    history.push("/vendre-un-produit")
  }

  return (
    <Fragment>
      <nav className="navbar navbar-expand-sm navbar-dark navbar-primary">
        <NavLink className="navbar-brand" to="/accueil">
          <img src={logo} width="50" height="40" alt="logo easyFind" />
        </NavLink>
        <NavLink className="navbar-brand" to="/accueil">
          EasyFind
        </NavLink>
        <button
          className="navbar-toggler d-lg-none bg-white"
          type="button"
          data-toggle="collapse"
          data-target="#collapsibleNavId"
          aria-controls="collapsibleNavId"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i className="fas fa-bars"></i>
        </button>
        <div className="collapse navbar-collapse" id="collapsibleNavId">
          <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
            <li className="nav-item mr-2" activeclassname="active">
              <NavLink className="nav-link" to="/accueil">
                <FontAwesomeIcon icon={faHome} /> Accueil
              </NavLink>
            </li>
            <li className="nav-item" activeclassname="active">
              <NavLink className="nav-link mr-2" to="/blog">
                Blog
              </NavLink>
            </li>
            <li className="nav-item" activeclassname="active">
              <NavLink className="nav-link mr-2" to="/a-propos">
                A propos
              </NavLink>
            </li>
            <li className="nav-item" activeclassname="active">
              <NavLink className="nav-link mr-2" to="/magasin">
                Magasin
              </NavLink>
            </li>
            <li className="nav-item" activeclassname="active">
              <NavLink className="nav-link" to="/contact">
                Contact
              </NavLink>
            </li>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            {!isAuthenticated && (
              <Fragment>
                <NavLink
                  className="btn btn-sm btn-rounded px-4 bg-white mr-sm-2 mr-2"
                  to="/connexion"
                >
                  Se connecter
                </NavLink>
                <NavLink
                  className="btn btn-sm btn-rounded px-4 bg-white my-2 my-sm-0"
                  to="/inscription"
                >
                  S'inscrire
                </NavLink>
              </Fragment>
            )}

            {isAuthenticated && (
              <Fragment>
                <button
                  type="button"
                  className="btn btn-sm btn-rounded px-4 bg-white my-2 my-sm-0 ml-2"
                  onClick={handleLogout}
                >
                  Se deconnecter
                </button>
                <button
                  type="button"
                  className="btn btn-sm btn-rounded px-4 bg-white my-2 my-sm-0 ml-2"
                  onClick={handleSaleProduct}
                >
                  Vendre
                </button>
              </Fragment>
            )}
          </form>
        </div>
      </nav>
    </Fragment>
  )
}

export default Navbar
