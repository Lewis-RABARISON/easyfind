import React from 'react'

function TextField({
  label,
  name,
  type = 'text',
  placeholder,
  value,
  onChange,
  error = '',
}) {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        type={type}
        className={`border-radius-50 form-control ${error ? 'is-invalid' : ''}`}
        id={name}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        error={error}
      />
      {error && <p className="invalid-feedback">{error}</p>}
    </div>
  )
}

export default TextField
