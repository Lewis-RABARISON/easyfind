import React from 'react'

function TextareaField({
  label,
  name,
  type = 'text',
  placeholder,
  value,
  onChange,
  error = '',
  children,
}) {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <textarea
        className={`border-radius-50 form-control ${error ? 'is-invalid' : ''}`}
        id={name}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        error={error}
      >
        {children}
      </textarea>
      {error && <p className="invalid-feedback">{error}</p>}
    </div>
  )
}

export default TextareaField
