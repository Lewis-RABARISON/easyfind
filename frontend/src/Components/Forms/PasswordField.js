import React, { Fragment } from 'react'
import { FaRegEye, FaRegEyeSlash } from 'react-icons/fa'

function PasswordField({
  label,
  name,
  type = 'text',
  placeholder,
  value,
  onChange,
  eye,
  setEye,
}) {
  const handleTogglePassword = () => setEye(!eye)

  return (
    <Fragment>
      <div className="form-group">
        <label htmlFor={name}>{label}</label>
        <div className="input-group">
          <input
            type={type}
            className="form-control border-radius-50"
            id={name}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
          />
          <div className="input-group-prepend">
            <span
              className="input-group-text bg-white rounded icon-toggle-password"
              onClick={handleTogglePassword}
            >
              {eye ? <FaRegEyeSlash /> : <FaRegEye />}
            </span>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default PasswordField
