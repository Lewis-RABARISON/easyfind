import { withRouter } from "react-router-dom";
import About from "../Pages/Front/About/About";
import Post from "../Pages/Front/Blog/Post/Post";
import Contact from "../Pages/Front/Contact/Contact";
import Home from "../Pages/Front/Home/Home";
import SaleProduct from "../Pages/Front/Sale/SaleProduct";
import ProductsList from "../Pages/Front/Store/ProductsList";
import Authentication from "../Pages/Front/User/Authentication";
import ForgottenPassword from "../Pages/Front/User/ForgottenPassword";
import Register from "../Pages/Front/User/Register";
import ResetPassword from "../Pages/Front/User/ResetPassword";

export const SITE_NAME = "EasyFind | ";
export const APP_URL = "https://127.0.0.1:8000";
export const LOGIN_URL = `${APP_URL}/api/login_check`;
export const REGISTER_URL = `${APP_URL}/api/users`;
export const FORGOTTEN_PASSWORD_URL = `${APP_URL}/api/check-email`;
export const PASSWORD_RESETTING_URL = `${APP_URL}/reset-password`;
export const POST_URL = `${APP_URL}/api/posts`;
export const PUBLISH_POST_URL = `${APP_URL}/api/posts/publish`;
export const SALE_PRODUCT_WITH_ROUTER = withRouter(SaleProduct)

export const PUBLIC_ROUTES = [
    {
        path: "/connexion",
        exact: true,
        component: Authentication
    },
    {
        path: "/inscription",
        exact: true,
        component: Register
    },
    {
        path: "/mot-de-passe-oublie",
        exact: true,
        component: ForgottenPassword
    },
    {
        path: "/reinitialisation-de-mot-de-passe/:token",
        exact: true,
        component: ResetPassword
    },
    {
        path: "/accueil",
        exact: true,
        component: Home
    },
    {
        path: "/a-propos",
        exact: true,
        component: About
    },
    {
        path: "/blog",
        exact: true,
        component: Post
    },
    {
        path: "/magasin",
        exact: true,
        component: ProductsList
    },
    {
        path: "/contact",
        exact: true,
        component: Contact
    },
    {
        path: "/vendre-un-produit",
        exact: true,
        component: SALE_PRODUCT_WITH_ROUTER
    },
];