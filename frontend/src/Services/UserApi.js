import axios from 'axios'
import {
  FORGOTTEN_PASSWORD_URL,
  LOGIN_URL,
  PASSWORD_RESETTING_URL,
  REGISTER_URL,
} from '../Utils/Global'
import jwtDecode from 'jwt-decode'

function headerToken(token) {
  axios.defaults.headers['Authorization'] = `Bearer ${token}`
}

function login(credentials) {
  return axios
    .post(LOGIN_URL, credentials)
    .then((response) => response.data.token)
    .then((token) => {
      window.localStorage.setItem('auth_token', token)
      headerToken(token)
    })
}

function logout() {
  window.localStorage.removeItem('auth_token')
  delete axios.defaults.headers['Authorization']
}

function validedToken() {
  let token = window.localStorage.getItem('auth_token')
  if (token) {
    let jwt_data = jwtDecode(token)
    let expiration_time = jwt_data.exp * 1000
    let current_time = new Date().getTime()
    if (current_time > expiration_time) {
      headerToken(token)
    } else {
      logout()
    }
  }
}

function isAuthenticated() {
  let token = window.localStorage.getItem('auth_token')
  if (token) {
    let jwt_data = jwtDecode(token)
    let expiration_time = jwt_data.exp * 1000
    let current_time = new Date().getTime()
    if (current_time > expiration_time) {
      return true
    }

    return false
  } else {
    return false
  }
}

function dataUser() {
  let token = window.localStorage.getItem('auth_token');
  if (token) {
    let jwt_data = jwtDecode(token)
    let expiration_time = jwt_data.exp * 1000
    let current_time = new Date().getTime()
      return jwt_data.userId
  } 
  else {
    return null
  }
}

function register(user_data) {
  return axios.post(REGISTER_URL, user_data)
}

function forgottenPassword(email) {
  return axios.get(`${FORGOTTEN_PASSWORD_URL}/${email}`)
}

function resetPassword(token) {
  return axios
    .get(`${PASSWORD_RESETTING_URL}/${token}`)
    .then((response) => response)
}

export default {
  login,
  logout,
  validedToken,
  isAuthenticated,
  register,
  forgottenPassword,
  resetPassword,
  dataUser
}
