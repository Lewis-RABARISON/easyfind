import axios from "axios";
import { POST_URL } from "../Utils/Global";

function findAll() { 
    return axios.get(POST_URL).then(response => response.data)
}

export default {
    findAll
}