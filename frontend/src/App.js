import React, { Fragment, useEffect, useState } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import Navbar from './Components/Navbar'
import 'bootstrap/dist/js/bootstrap'
import { BrowserRouter as Router, Route, withRouter } from 'react-router-dom'
import { PUBLIC_ROUTES } from './Utils/Global'
import AuthenticationContext from './Contexts/AuthenticationContext'
import UserApi from './Services/UserApi'
import Footer from './Pages/Front/Footer/Footer'

UserApi.validedToken()

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(
    UserApi.isAuthenticated(),
  )
  const NavbarWithRouter = withRouter(Navbar)
  const context_auth = {
    isAuthenticated,
    setIsAuthenticated,
  }

  return (
    <Fragment>
      <AuthenticationContext.Provider value={context_auth}>
        <Router>
          <NavbarWithRouter />
          {PUBLIC_ROUTES.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={route.component}
            />
          ))}
        </Router>
      </AuthenticationContext.Provider>
      <Footer />
    </Fragment>
  )
}

export default App
