import React, { useEffect } from 'react'
import { SITE_NAME } from '../../../Utils/Global'

function Home() {

  useEffect(() => {
    document.title = `${SITE_NAME} Accueil`
  }, [])

  return (
    <div>Accueil</div>
  )
}

export default Home