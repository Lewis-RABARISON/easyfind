import React, { useEffect } from 'react'
import { SITE_NAME } from '../../../Utils/Global'

function About() {

  useEffect(() => {
    document.title = `${SITE_NAME} A propos`
  }, [])

  return (
    <div>About</div>
  )
}

export default About