import React, { useEffect } from 'react'
import { SITE_NAME } from '../../../Utils/Global'

function Contact() {

  useEffect(() => {
    document.title = `${SITE_NAME} Contact`
  }, [])

  return (
    <div>Contact</div>
  )
}

export default Contact