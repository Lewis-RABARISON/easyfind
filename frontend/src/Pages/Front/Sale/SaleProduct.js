import axios from 'axios'
import React, { Fragment, useContext, useEffect, useState } from 'react'
import FileField from '../../../Components/Forms/FileField'
import TextareaField from '../../../Components/Forms/TextareaField'
import TextField from '../../../Components/Forms/TextField'
import { PUBLISH_POST_URL } from '../../../Utils/Global'
import UserApi from '../../../Services/UserApi'

function SaleProduct({ history }) {
  const [product, setProduct] = useState({
    postTitle: '',
    postDescription: '',
  })

  const [imageFile, setImageFile] = useState()

  const [errors, setErrors] = useState({
    postTitle: '',
    imageFile: '',
  })

  const handleChangeField = ({ target }) => {
    let { name, value } = target
    setProduct({ ...product, [name]: value })
  }

  const handleFile = ({ target }) => {
    setImageFile(target.files[0])
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    let form_data = new FormData()
    form_data.append('postTitle', product.postTitle)
    form_data.append('postDescription', product.postDescription)
    form_data.append('imageFile', imageFile)
    const config = {
      headers: {
        'content-type': 'multipart/formdata',
      },
    }

    try {
      await axios.post(PUBLISH_POST_URL, form_data, config)
      history.replace('/magasin')
    } catch ({ response }) {
      // console.log(response)
      let api_errors = {}
      if (typeof imageFile !== 'undefined') {
        let { violations } = response.data
        violations.forEach(({ propertyPath, message }) => {
          api_errors[propertyPath] = message
        })
        setErrors(api_errors)
      } 
      else if (product.postTitle == "" && typeof imageFile === 'undefined'){
        api_errors['postTitle'] = 'Veuillez saisir le nom du produit'
        api_errors['imageFile'] = 'Veuillez mettre le photo du produit'
        setErrors(api_errors)
      }
      else {
        api_errors['imageFile'] = 'Veuillez mettre le photo du produit'
        setErrors(api_errors)
      }
    }
  }

  return (
    <Fragment>
      <div className="container">
        <div className="row">
          <div className="col-md-6 mb-2">
            <form onSubmit={handleSubmit}>
              <TextField
                name="postTitle"
                placeholder="Entrer le nom du produit"
                value={product.postTitle}
                onChange={handleChangeField}
                error={errors.postTitle}
              />
              <TextareaField
                name="postDescription"
                placeholder="Entrer le description"
                value={product.postDescription}
                onChange={handleChangeField}
              ></TextareaField>
              <FileField name="imageFile" onChange={handleFile} error={errors.imageFile} />
              <button
                type="submit"
                className="btn btn-primary btn-rounded px-5"
              >
                Enregistrer
              </button>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default SaleProduct
