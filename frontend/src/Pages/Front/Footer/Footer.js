import React from 'react'
import { SITE_NAME } from '../../../Utils/Global'
import "./Footer.css"

function Footer() {
  return (
    <footer className='text-center bg-primary py-3 text-white'>
        {SITE_NAME} Copyright &copy; 2022
    </footer>
  )
}

export default Footer