import React, { useEffect, useState } from 'react'
import PostApi from '../../../Services/PostApi'
import { SITE_NAME } from '../../../Utils/Global'
import ProductItem from './ProductItem'
import './Product.css'

function ProductsList() {
  const [products, setProducts] = useState([])

  /**
   * Recupere les produits
   */
  const fetchProducts = async () => {
    try {
      let data = await PostApi.findAll()
      setProducts(data)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    document.title = `${SITE_NAME} Magasin`
    /**
     * Recupere les produits
     */
    fetchProducts()
  }, [])

  return (
    <div className="container-fluid d-flex flex-row">
      <div className="w-75 d-flex flex-wrap">
        {products.map((product) => (
          <ProductItem product={product} key={product.id} />
        ))}
      </div>
      <div className="w-25 mt-3">
          <div className="card">
              <div className="card-header bg-success text-white">
                  Catégories
              </div>
              <div className="card-body">
                  <ul className='list-unstyled'>
                      <li>Ordinateur</li>
                      <li>Telephone</li>
                      <li>Tablette</li>
                  </ul>
              </div>
          </div>
      </div>
    </div>
  )
}

export default ProductsList
