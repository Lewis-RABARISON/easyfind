import React, { Fragment } from 'react'
import { APP_URL } from '../../../Utils/Global'

function ProductItem({ product }) {
  return (
    <Fragment>
      <div className="card w-30 m-3 card-product border-0">
        <img
          className="card-img-top card-img-product"
          src={APP_URL + '' + product.postImage}
          alt=""
        />
        <div className="card-body">
          <h4 className="card-title">{product.postTitle}</h4>
        </div>
        <div className='btn-view-product'>
          <button type="button" className="btn btn-primary btn-rounded px-5">Voir</button>
        </div>
      </div>
    </Fragment>
  )
}

export default ProductItem
