import React, { Fragment, useContext, useEffect, useState } from 'react'
import './User.css'
import logo from './logo.svg'
import { FaRegEye, FaRegEyeSlash } from 'react-icons/fa'
import UserApi from '../../../Services/UserApi'
import { SITE_NAME } from '../../../Utils/Global'
import AuthenticationContext from '../../../Contexts/AuthenticationContext'
import { Link } from 'react-router-dom'
// import $ from "jquery"

function Authentification({ history }) {
  useEffect(() => {
    document.title = `${SITE_NAME} Authentification`
  }, [])

  const [eye, setEye] = useState(true)
  const handleTogglePassword = () => setEye(!eye)
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  })
  const [error, setError] = useState(false)
  const { setIsAuthenticated} = useContext(AuthenticationContext)

  async function handleSubmitFormAuth(event) {
    event.preventDefault()
    try {
      await UserApi.login(credentials)
      setIsAuthenticated(true)
      setError(false)
      history.replace('/accueil')
    } catch (error) {
      setError(true)
    }
  }

  function handleChangeField({ currentTarget }) {
    const { name, value } = currentTarget
    setCredentials({ ...credentials, [name]: value })
  }

  return (
    <Fragment>
      <div className="bg-user"></div>
      <div className="container card-user">
        <div className="card mt-5">
          <div>
            <h4 className="text-center mt-3">Authentification</h4>
            <div className="card-image card-image-logo">
              <img src={logo} alt="logo easyFind" className="logo-easyFind" />
            </div>
            {error && (
              <div className="alert bg-danger text-white mx-3">
                Votre email ou votre mot de passe est incorrecte
              </div>
            )}
          </div>
          <div className="card-body">
            <form noValidate onSubmit={handleSubmitFormAuth}>
              <div>
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  className="border-radius-50 form-control"
                  id="email"
                  name="username"
                  placeholder="Entrez votre adresse mail"
                  value={credentials.username}
                  onChange={handleChangeField}
                />
              </div>
              <div className="mt-2">
                <label htmlFor="password">Mot de passe</label>
                <div className="input-group">
                  <input
                    type={eye ? "password" : "text"}
                    className="form-control border-radius-50"
                    id="password"
                    name="password"
                    placeholder="Entrez votre mot de passe"
                    value={credentials.password}
                    onChange={handleChangeField}
                  />
                  <div className="input-group-prepend">
                    <span
                      className="input-group-text bg-white rounded icon-toggle-password"
                      onClick={handleTogglePassword}
                    >
                      {eye ? <FaRegEyeSlash /> : <FaRegEye />}
                    </span>
                  </div>
                </div>
              </div>
              <div className="d-flex align-items-center justify-content-center">
                <button
                  type="submit"
                  className="btn btn-primary btn-user btn-block mt-2 border-radius-50"
                >
                  Se connecter
                </button>
              </div>
              <div className="mt-1">
                <Link className="text-primary" to="/mot-de-passe-oublie">
                  Mot de passe oublié
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Authentification
