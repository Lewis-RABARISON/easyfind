import React, { Fragment, useEffect, useState } from 'react'
import { SITE_NAME } from '../../../Utils/Global'
import logo from './logo.svg'
import TextField from '../../../Components/Forms/TextField'
import PasswordField from '../../../Components/Forms/PasswordField'
import UserApi from '../../../Services/UserApi'

function Register({history}) {
  useEffect(() => {
    document.title = `${SITE_NAME} Inscription`
  }, [])

  const [user, setUser] = useState({
    userFirstname: '',
    userLastname: '',
    userEmail: '',
    password: '',
  })

  const [eye, setEye] = useState(true)
  const [errors, setErrors] = useState({})

  const handleTogglePassword = () => setEye(!eye)

  async function handleSubmitFormRegister(event) {
    event.preventDefault()
    try {
      await UserApi.register(user) 
      setErrors({})
      history.replace("/connexion")
    } catch ({response}) {
      let api_errors = {};
      let {violations} = response.data;
      violations.forEach(({propertyPath, message}) => {
        api_errors[propertyPath] = message;
      });
      setErrors(api_errors);
    }
  }

  function handleChangeField({ currentTarget }) {
    let { name, value } = currentTarget
    setUser({ ...user, [name]: value })
  }

  return (
    <Fragment>
      <div className="bg-user"></div>
      <div className="container card-user">
        <div className="card mt-5">
          <div>
            <h4 className="text-center mt-3">Inscription</h4>
          </div>
          <div className="card-body">
            <form noValidate onSubmit={handleSubmitFormRegister}>
              <TextField
                label="Email"
                name="userEmail"
                value={user.userEmail}
                placeholder="Entrez votre adresse mail"
                onChange={handleChangeField}
                error={errors.userEmail}
              />
              <TextField
                label="Nom"
                name="userLastname"
                value={user.userLastname}
                placeholder="Entrez votre nom"
                onChange={handleChangeField}
                error={errors.userLastname}
              />
              <TextField
                label="Prenom"
                name="userFirstname"
                value={user.userFirstname}
                placeholder="Entrez votre prenom"
                onChange={handleChangeField}
                error={errors.userFirstname}
              />
              <PasswordField
                label="Mot de passe"
                type={eye ? 'password' : 'text'}
                name="password"
                value={user.password}
                placeholder="Entrez votre mot de passe"
                onChange={handleChangeField}
                error={errors.password}
                eye={eye}
                setEye={setEye}
              />
              <div className="d-flex align-items-center justify-content-center">
                <button
                  type="submit"
                  className="btn btn-primary btn-user btn-block mt-2 border-radius-50"
                >
                  S'inscrire
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Register
