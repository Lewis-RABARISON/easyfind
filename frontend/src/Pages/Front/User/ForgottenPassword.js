import React, { Fragment, useState } from 'react'
import { Link } from 'react-router-dom'
import TextField from '../../../Components/Forms/TextField'
import UserApi from '../../../Services/UserApi'
import logo from './logo.svg'

function ForgottenPassword() {
  const [email, setEmail] = useState('')
  const [successMessage, setSuccessMessage] = useState('')
  const [error, setError] = useState('')
  const handleSubmitFormForgottenPassword = async (event) => {
    event.preventDefault()
    try {
      await UserApi.forgottenPassword(email).then((response) =>
        setSuccessMessage(response.data.message),
      )
      setError('')
    } catch (error) {
      setError("L'email n'existe pas")
    }
  }

  const handleChangeField = ({ currentTarget }) => {
    setEmail(currentTarget.value)
  }

  return (
    <Fragment>
      <div className="bg-user"></div>
      <div className="container card-user">
        <div className="card mt-5">
          <div>
            <h4 className="text-center mt-3">Mot de passe oublié</h4>
            <div className="card-image card-image-logo">
              <img src={logo} alt="logo easyFind" className="logo-easyFind" />
            </div>
            {successMessage && (
              <div className="alert bg-success text-white mx-3">
                {successMessage}
              </div>
            )}
          </div>
          <div className="card-body">
            <form noValidate onSubmit={handleSubmitFormForgottenPassword}>
              <TextField
                label="Email"
                name="email"
                value={email}
                placeholder="Entrez votre adresse mail"
                onChange={handleChangeField}
                error={error}
              />
              <div className="d-flex align-items-center justify-content-center">
                <button
                  type="submit"
                  className="btn btn-primary btn-user btn-block mt-2 border-radius-50"
                >
                  Envoyer
                </button>
              </div>
              <div className="mt-1">
                <Link className="text-primary" to="/connexion">
                  Retourner
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default ForgottenPassword
