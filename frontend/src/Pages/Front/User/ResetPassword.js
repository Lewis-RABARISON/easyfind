import React, { useState } from 'react'
import UserApi from '../../../Services/UserApi'

function ResetPassword({ match }) {
  let token = match.params.token
  const [isValidToken, setIsValidToken] = useState(false)

  async function checkToken() {
    try {
      await UserApi.resetPassword(token)
      setIsValidToken(true)
      alert("try")
    } catch (error) {
      console.log(error)
      setIsValidToken(false)
      alert("catch")
    }
  }

  return isValidToken
}

export default ResetPassword
