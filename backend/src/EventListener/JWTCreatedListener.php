<?php

declare(strict_types=1);

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

class JWTCreatedListener
{
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $data = $event->getData();
        $user_id = $event->getUser()->getId();
        $data['userId'] = $user_id;
        $data['firstname'] = $event->getUser()->getUserFirstname();
        $data['lastname'] = $event->getUser()->getUserLastname();
        $event->setData($data);
    }
}