<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Post;
use App\Utils\Globals;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\String\Slugger\SluggerInterface;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private SluggerInterface $slugger
    ) {
    }

    /**
     * @param ObjectManager $manager
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < count(Globals::POST); $i++) {
            $post = new Post();
            $post_title = Globals::POST[$i]["postTitle"];
            $post->setPostTitle($post_title);
            $post->setPostSlug((string) $this->slugger->slug($post_title));
            $post->setPostCreatedAt(new \DateTimeImmutable());
            $post->setPostDescription(Globals::POST[$i]["postDescription"]);
            $post->setPostAuthor($this->getReference("author_".$faker->numberBetween(0, 10)));
            $post->setPostImage(Globals::POST[$i]["postImage"]);
            $manager->persist($post);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
