<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 * @author Lewis
 */
class UserFixtures extends Fixture
{
    public const POST_AUTHOR = "post-author";

    public function __construct(private UserPasswordHasherInterface $password_hasher)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadUserSuperadmin($manager);
        $this->loadUser($manager);
    }

    /**
     * Charge l'utilisateur superadmin
     * @param ObjectManager $manager
     */
    public function loadUserSuperadmin(ObjectManager $manager): void
    {
        $user_superadmin = new User();
        $user_superadmin->setUserFirstname("Lewis");
        $user_superadmin->setUserLastname("RABARISON");
        $user_superadmin->setUserEmail("rabarison.lewis@yahoo.com");
        $user_superadmin->setRoles(["ROLE_SUPERADMIN"]);
        $user_superadmin->setPassword($this->password_hasher->hashPassword($user_superadmin, 'password'));

        $manager->persist($user_superadmin);
        $manager->flush();

        $this->addReference("author_0", $user_superadmin);
    }

    /**
     * Charge les utilisateurs simple
     * @param ObjectManager $manager
     */
    public function loadUser(ObjectManager $manager): void
    {
        $faker = Factory::create();
        for ($i=1; $i < 11; $i++) {
            $user = new User();
            $gender = ['man','woman'];
            $gender = $gender[array_rand($gender)];
            $lastname = $faker->lastName();
            $user->setUserFirstname($faker->firstName($gender));
            $user->setUserLastname($lastname);
            $user->setUserEmail($faker->email());
            $user->setRoles(["ROLE_USER"]);
            $user->setPassword($this->password_hasher->hashPassword($user, 'password'));

            $manager->persist($user);
            $manager->flush();

            $this->addReference("author_$i", $user);
        }
    }
}
