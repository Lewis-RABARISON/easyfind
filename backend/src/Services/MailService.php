<?php

declare(strict_types=1);

namespace App\Services;

use App\Utils\Globals;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MailService
{
    public function __construct(
        private MailerInterface $mailer,
        private string $email_sender,
        private string $name_sender
    ) {
    }

    public function send(string $email_recipient, string $url_reset_password): void
    {
        $email = (new TemplatedEmail())
            ->from(new Address($this->email_sender, $this->name_sender))
            ->to($email_recipient)
            ->subject(GLOBALS::SUBJECT_EMAIL_RESET_PASSWORD)
            ->htmlTemplate("email/reset_password.html.twig")
            ->context(compact('url_reset_password'))
        ;

        $this->mailer->send($email);
    }
}
