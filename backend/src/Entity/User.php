<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\UserController;
use App\Entity\Post;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ["userEmail"])]
#[ApiResource(
    formats: ["json"],
    itemOperations: [
        'get',
        'put',
        'patch',
        'delete',
        'change-password' => [
            'method' => Request::METHOD_PATCH,
            'path' => '/users/{id}/change-password',
            'controller' => UserController::class,
            'validate' => false
        ]
    ]
)]

/**
 * Class User
 * @package App\Entity
 * @author Lewis
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Email]
    #[Assert\Length(min:4, max:180)]
    private string $userEmail;

    #[ORM\Column(type: 'json')]
    private array $userRoles = [];

    #[ORM\Column(type: 'string')]
    #[Assert\Length(min:8, max:255)]
    private string $password;

    #[ORM\Column(type: 'string', length: 150, nullable: true)]
    #[Assert\NotBlank]
    #[Assert\Length(min:2, max:150)]
    private ?string $userFirstname;

    #[ORM\Column(type: 'string', length: 80)]
    #[Assert\NotBlank]
    #[Assert\Length(min:2, max:80)]
    private string $userLastname;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $userToken;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $userTokenCreatedAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $userTokenExpiredAt;

    #[ORM\OneToMany(mappedBy: 'postAuthor', targetEntity: Post::class)]
    private $author;

    public function __construct()
    {
        $this->author = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserEmail(): ?string
    {
        return $this->userEmail;
    }

    public function setUserEmail(string $userEmail): self
    {
        $this->userEmail = $userEmail;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->userEmail;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->userEmail;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->userRoles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $userRoles): self
    {
        $this->userRoles = $userRoles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserFirstname(): ?string
    {
        return $this->userFirstname;
    }

    public function setUserFirstname(?string $userFirstname): self
    {
        $this->userFirstname = $userFirstname;

        return $this;
    }

    public function getUserLastname(): ?string
    {
        return $this->userLastname;
    }

    public function setUserLastname(string $userLastname): self
    {
        $this->userLastname = $userLastname;

        return $this;
    }

    public function getUserToken(): ?string
    {
        return $this->userToken;
    }

    public function setUserToken(?string $userToken): self
    {
        $this->userToken = $userToken;

        return $this;
    }

    public function getUserTokenCreatedAt(): ?\DateTimeImmutable
    {
        return $this->userTokenCreatedAt;
    }

    public function setUserTokenCreatedAt(?\DateTimeImmutable $userTokenCreatedAt): self
    {
        $this->userTokenCreatedAt = $userTokenCreatedAt;

        return $this;
    }

    public function getUserTokenExpiredAt(): ?\DateTimeInterface
    {
        return $this->userTokenExpiredAt;
    }

    public function setUserTokenExpiredAt(?\DateTimeInterface $userTokenExpiredAt): self
    {
        $this->userTokenExpiredAt = $userTokenExpiredAt;

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getAuthor(): Collection
    {
        return $this->author;
    }

    public function addAuthor(Post $author): self
    {
        if (!$this->author->contains($author)) {
            $this->author[] = $author;
            $author->setPostAuthor($this);
        }

        return $this;
    }

    public function removeAuthor(Post $author): self
    {
        if ($this->author->removeElement($author)) {
            // set the owning side to null (unless already changed)
            if ($author->getPostAuthor() === $this) {
                $author->setPostAuthor(null);
            }
        }

        return $this;
    }
}
