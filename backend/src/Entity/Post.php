<?php

namespace App\Entity;

use App\Controller\PostController;
use App\Entity\User;
use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ORM\HasLifecycleCallbacks]
/**
 * @Vich\Uploadable
 */
#[ApiResource(
    formats: ["json"],
//    normalizationContext: ['groups' => ['post_read']],
//    denormalizationContext: ['groups' => ['post_write']],
    collectionOperations: [
        "get",
        "post",
        "publish" => [
            "method" => Request::METHOD_POST,
            "path" => "/posts/publish",
            "controller" => PostController::class,
        ]
    ],
    itemOperations: [
        "get",
        "put",
        "patch",
        "delete"
    ],
    attributes: [
        "pagination_enabled" => false
    ]
)]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 70)]
    #[Assert\NotBlank]
    #[Assert\Length(min: 2, max: 70)]
    private string $postTitle;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Assert\Length(max: 1000)]
    private ?string $postDescription;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'author')]
    #[ORM\JoinColumn(nullable: false)]
    private User $postAuthor;

    #[ORM\Column(type: 'datetime_immutable')]
    private \DateTimeImmutable $postCreatedAt;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(max: 255)]
    private string $postSlug;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $postUpdatedAt;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    public $postImage;

    /**
     * @Vich\UploadableField(mapping="blog", fileNameProperty="postImage")
     */
    #[Assert\NotBlank]
    public ?File $imageFile;

    public function __construct()
    {
        $this->postCreatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostTitle(): ?string
    {
        return $this->postTitle;
    }

    public function setPostTitle(string $postTitle): self
    {
        $this->postTitle = $postTitle;

        return $this;
    }

    public function getPostDescription(): ?string
    {
        return $this->postDescription;
    }

    public function setPostDescription(?string $postDescription): self
    {
        $this->postDescription = $postDescription;

        return $this;
    }

    public function getPostAuthor(): User
    {
        return $this->postAuthor;
    }

    public function setPostAuthor(User $postAuthor): self
    {
        $this->postAuthor = $postAuthor;

        return $this;
    }

    public function getPostCreatedAt(): ?\DateTimeImmutable
    {
        return $this->postCreatedAt;
    }

    public function setPostCreatedAt(\DateTimeImmutable $postCreatedAt): self
    {
        $this->postCreatedAt = $postCreatedAt;

        return $this;
    }

    public function getPostSlug(): ?string
    {
        return $this->postSlug;
    }

    public function setPostSlug(string $postSlug): self
    {
        $this->postSlug = $postSlug;

        return $this;
    }

    public function getPostUpdatedAt(): ?\DateTimeInterface
    {
        return $this->postUpdatedAt;
    }

    public function setPostUpdatedAt(?\DateTimeInterface $postUpdatedAt): self
    {
        $this->postUpdatedAt = $postUpdatedAt;

        return $this;
    }

    public function getPostImage(): ?string
    {
        return $this->postImage;
    }

    public function setPostImage(?string $postImage): self
    {
        $this->postImage = $postImage;

        return $this;
    }

    /**
     * @param File|null $imageFile
     * @return $this
     */
    public function setImageFile(?File $imageFile = null): Post
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->postUpdatedAt = new \DateTime("now");
        }

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile ?? null;
    }

    #[ORM\PostPersist]
    public function postPersist(): void
    {
        $this->imageFile = null;
    }
}
