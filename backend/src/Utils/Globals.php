<?php

declare(strict_types=1);

namespace App\Utils;

class Globals
{
    public const EMAIL_CONFIRMATION_MESSAGE = "Votre addresse email est verifié,un email vous a été envoyé pour réinitialiser votre mot de passe, veuillez consulter votre boite mail";
    public const EMAIL_NOT_FOUND = "L'email n'existe pas";
    public const SUBJECT_EMAIL_RESET_PASSWORD = "Réinitialisation de mot de passe";
    public const LINK_NOT_VALID = "Ce lien n'est plus valide";
    public const TOKEN_VALID = "Le token est valide";
    public const TOKEN_EXPIRED = "Le token est expiré";
    public const POST = [
        [
            "postTitle" => "Acer Aspire 5 A517-52-31FU",
            "postDescription" => "Puissance et productivité, c'est ce que propose le PC 
                                portable Acer Aspire 5 A517-52-31FU avec son processeur Intel Core i3-1115G4, 
                                ses 8 Go de RAM et son SSD 256 Go. Idéal pour 
                                les professionnels comme pour les particuliers exigeants, 
                                il est efficace et polyvalent.",
            "postImage" => "6259ac7a9d409772462973.png"
        ],
        [
            "postTitle" => "Acer nitro 5",
            "postDescription" => "Système d'exploitation : Windows 11 Famille
                                Processeur Intel® Core™ i71 de 12e génération en configuration maximale
                                Carte graphique pour ordinateur portable
                                GeForce RTX™ série 30 1 en configuration maximale
                                Mémoire DDR4 32 Go, 3200 MHz en configuration maximale 2 
                                emplacements PCIe M.2, Raid 2 To Gen 4",
            "postImage" => "6259b63f383a0899313325.png"
        ],
        [
            "postTitle" => "Acer Aspire 5 A517-52-31FU",
            "postDescription" => "Le PC Portable Acer TravelMate P2 P215-41 est conçu et pensé 
                                pour le monde de l'entreprise. Avec des composants performants et un soin particulier
                                 apporté à la mobilité, il a pour vocation de vous permettre de rester productif et 
                                 efficace en toutes circonstances.",
            "postImage" => "6259b64b29ca0861526978.png"
        ],
        [
            "postTitle" => "Asus_VivoBook_D509DA-EJ102T",
            "postDescription" => "HDD 500Go, Ram 4Go, Processeur corei5",
            "postImage" => "6259ac7a9d409772462974.png"
        ],
        [
            "postTitle" => "ASUS-E402",
            "postDescription" => "HDD 500Go, Ram 4Go, Processeur corei5",
            "postImage" => "6259b64b29ca0861526979.png"
        ],
        [
            "postTitle" => "HP Pavilion Laptop 15 eg0616nia 67P20EA",
            "postDescription" => "HDD 500Go, Ram 4Go, Processeur corei5",
            "postImage" => "6259b64b29ca0861526980.png"
        ],
        [
            "postTitle" => "Lenovo ThinkPad E15 Gen 2",
            "postDescription" => "HDD 500Go, Ram 4Go, Processeur corei5",
            "postImage" => "6259b64b29ca0861526981.png"
        ],
        [
            "postTitle" => "Vivobook1 4 K413",
            "postDescription" => "HDD 500Go, Ram 4Go, Processeur corei5",
            "postImage" => "6259b64b29ca0861526982.png"
        ]
    ];
}
