<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserDataPersister
 * @package App\DataPersister
 * @author Lewis
 */
class UserDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private UserPasswordHasherInterface $password_hasher,
        private EntityManagerInterface $entity_manager
    )
    {
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return bool
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return object|void
     */
    public function persist($data, array $context = [])
    {
        $data->setPassword($this->password_hasher->hashPassword($data, $data->getPassword()));
        $this->entity_manager->persist($data);
        $this->entity_manager->flush();
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return mixed
     */
    public function remove($data, array $context = [])
    {
    }
}
