<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RequestStack;

class PostDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(
        private EntityManagerInterface $entity_manager,
        private SluggerInterface $slugger,
        private Security $security,
        private RequestStack $request
    )
    {
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return bool
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Post;
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return object|void
     */
    public function persist($data, array $context = [])
    {
        $data->setPostSlug((string) $this->slugger->slug($data->getPostTitle()));
        $data->setPostAuthor($this->security->getUser());
        if ($this->request->getCurrentRequest()->getMethod() == Request::METHOD_PATCH) {
            $data->setPostUpdatedAt(new \DateTime());
        }

        $this->entity_manager->persist($data);
        $this->entity_manager->flush();
    }

    /**
     * @param mixed $data
     * @param array $context
     * @return mixed
     */
    public function remove($data, array $context = [])
    {
        $this->entity_manager->remove($data);
        $this->entity_manager->flush();
    }
}
