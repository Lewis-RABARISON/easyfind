<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    public function __invoke(Request $request): Post
    {
        $post = new Post();
        $uploaded_file = $request->files->get('imageFile');
        $post->setPostTitle($request->attributes->get("data")->getPostTitle());
        $post->setPostDescription($request->attributes->get("data")->getPostDescription());

        if (!$uploaded_file) {
            throw new BadRequestHttpException('"file" is required');
        }

        $post->imageFile = $uploaded_file;

        return $post;
    }
}
