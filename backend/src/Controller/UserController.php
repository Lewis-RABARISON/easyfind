<?php

namespace App\Controller;

use ApiPlatform\Core\Api\UrlGeneratorInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Utils\Globals;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use App\Services\MailService;

class UserController extends AbstractController
{
    public const PATH_RESET_TOKEN = "reset_password";

    public function __construct(
        private UserRepository $user_repository,
        private MailService $mailer,
        private TokenGeneratorInterface $token_generator,
        private EntityManagerInterface $entity_manager,
        private UserPasswordHasherInterface $password_hasher,
    ) {
    }

    /**
     * Verifie l'email de l'tilisateur et envoie un email de reinitialisation de mot de passe
     * @param string $email
     * @return JsonResponse
     */
    #[Route('/api/check-email/{email}', name: 'check_email')]
    public function checkEmail(string $email): JsonResponse
    {
        /* @var User $user */
        $user = $this->user_repository->findOneBy(['userEmail' => $email]);

        if (is_null($user)) {
            $data = [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Globals::EMAIL_NOT_FOUND
            ];

            return new JsonResponse($data, Response::HTTP_NOT_FOUND);
        } else {
            $data = [
                'code' => Response::HTTP_ACCEPTED,
                'message' => Globals::EMAIL_CONFIRMATION_MESSAGE
            ];

            $token = $this->token_generator->generateToken();
            $url_reset_password = "http://localhost:3000/reinitialisation-de-mot-de-passe/{$token}";

            $user->setUserToken($token);
            $user->setUserTokenCreatedAt(new \DateTimeImmutable());
            $user->setUserTokenExpiredAt((new \DateTime())->modify('+30 minute'));
            $this->entity_manager->flush();

            $this->mailer->send($email, $url_reset_password);

            return new JsonResponse($data, Response::HTTP_ACCEPTED);
        }
    }

    /**
     * Verifie le token(valide,invalide,expire)
     * @param string $token
     * @return JsonResponse
     */
    #[Route('/reset-password/{token}', name: 'reset_password')]
    public function resetPassword(string $token)
    {
        /* @var User $user */
        $user = $this->user_repository->findOneBy(['userToken' => $token]);

        if (is_null($user)) {
            $data = [
                'code' => Response::HTTP_NOT_FOUND,
                'message' => Globals::LINK_NOT_VALID
            ];

            return new JsonResponse($data, Response::HTTP_NOT_FOUND);
        } else {
            $current_date = new \DateTime();
            if ($current_date > $user->getUserTokenExpiredAt()) {
                $data = [
                    'code' => Response::HTTP_GONE,
                    'message' => Globals::TOKEN_EXPIRED
                ];

                return new JsonResponse($data, Response::HTTP_GONE);
            } else {
                $data = [
                    'code' => Response::HTTP_ACCEPTED,
                    'message' => Globals::TOKEN_VALID
                ];

                return new JsonResponse($data, Response::HTTP_ACCEPTED);
            }
        }
    }

    /**
     * Permet de change le mot de passe
     * @param User $data
     */
    public function __invoke(User $data): void
    {
        $data->setPassword($this->password_hasher->hashPassword($data, $data->getPassword()));
        $data->setUserToken(null);
        ;
        $this->entity_manager->flush();
    }
}
