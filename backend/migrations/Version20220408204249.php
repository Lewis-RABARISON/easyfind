<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220408204249 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, post_author_id INT NOT NULL, post_title VARCHAR(70) NOT NULL, post_description LONGTEXT DEFAULT NULL, INDEX IDX_5A8A6C8D571B8DEC (post_author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D571B8DEC FOREIGN KEY (post_author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user ADD user_token VARCHAR(255) DEFAULT NULL, ADD user_token_created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD user_token_expired_at DATETIME DEFAULT NULL, DROP username');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE post');
        $this->addSql('ALTER TABLE user ADD username VARCHAR(70) NOT NULL, DROP user_token, DROP user_token_created_at, DROP user_token_expired_at');
    }
}
